package models;

import models.playables.GrudgerMoveBehaviour;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class GrudgerMoveBehaviourTest {

    @Test
    public void shouldBeAbleToInstantiateGrudger() {
        //given
        GrudgerMoveBehaviour behaviour;

        //when
        behaviour = new GrudgerMoveBehaviour();

        //then
        Assert.assertNotNull(behaviour);
    }

    @Test
    public void shouldCooperateForTheFirstTime() {
        //given
        GrudgerMoveBehaviour behaviour = new GrudgerMoveBehaviour();
        Move expected = Move.COOPERATE;

        //when
        Move actual = behaviour.getCurrentMove();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldCheatOnceGetCheated() {
        //given
        GrudgerMoveBehaviour behaviour = new GrudgerMoveBehaviour();
        Game game = mock(Game.class);
        Move expected = Move.CHEAT;

        //when
        behaviour.update(game, new Move[]{Move.CHEAT, Move.COOPERATE});
        Move actual = behaviour.getCurrentMove();

        //then
        Assert.assertEquals(expected, actual);
    }

}
