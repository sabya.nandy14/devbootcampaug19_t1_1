package models.playables;

import models.Move;
import models.MoveBehaviour;

import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

public class GrudgerMoveBehaviour implements MoveBehaviour, Observer {

    private Move nextMove = Move.COOPERATE;

    @Override
    public Move getCurrentMove() {
        return nextMove;
    }

    @Override
    public void update(Observable game, Object moves) {
        if (moves instanceof Move[]) {
            Move[] lastMoves = (Move[]) moves;

            if (lastMoves.length == 2) {
                nextMove = Arrays.stream(lastMoves).anyMatch(m -> m == Move.CHEAT) ? Move.CHEAT : Move.COOPERATE;
            }
        }
    }
}
