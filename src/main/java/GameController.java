import models.*;
import models.playables.ConsoleMoveBehaviour;

public class GameController {

    public static void main(String[] args) {

        runGameWithConsolePlayers();
        runGameWithBehaviourPlayers();
    }

    private static void runGameWithConsolePlayers() {
        ScannerUserInput scannerUserInput = new ScannerUserInput();

        Game game = new Game(new Player(new ConsoleMoveBehaviour(scannerUserInput)), new Player(new ConsoleMoveBehaviour(scannerUserInput)), new Machine(), 2, new ConsoleIO() {
            @Override
            public void printString(String output) {
                System.out.println(output);
            }
        });

        game.play();
    }

    private static void runGameWithBehaviourPlayers() {

        Game game = new Game(new Player(() -> Move.COOPERATE), new Player(() -> Move.CHEAT), new Machine(), 5, new ConsoleIO() {
            @Override
            public void printString(String x) {
                System.out.println(x);
            }
        });

        game.play();
    }
}
